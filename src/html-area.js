const resolve = import.meta.resolve ?? (path => new URL(path, import.meta.url));

const css = resolve('./html-area.css');

export default class HtmlArea extends HTMLElement {
	static get formAssociated(){ return true; }

	_syncFormValue = null;

	constructor(){
		super();
		const internals = this.attachInternals();
		this._syncFormValue = () => {
			internals.setFormValue(this.innerHTML);
		}

		const shadow = this.attachShadow({ mode: 'closed' });
		shadow.innerHTML = `<link rel="stylesheet" href="${css}"><slot></slot>`;

		this.addEventListener('input', this._syncFormValue);
	}

	connectedCallback(){
		this.setAttribute('contenteditable', true)

		const ta = this.firstElementChild;
		if(ta && ta.type === 'textarea'){
			if(!this.hasAttribute('name')) {
				this.setAttribute('name', ta.name);
			}
			this.innerHTML = ta.value;
		} else {
			this._syncFormValue();
		}
	}

	get innerHTML(){
		return super.innerHTML;
	}

	set innerHTML(value){
		super.innerHTML = value;
		this._syncFormValue();
	}

	static register(name = 'html-area'){
		customElements.define(name, HtmlArea)
	}
}
