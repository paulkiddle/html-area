import { serve } from "https://deno.land/std/http/server.ts";
import { serveFile } from "https://deno.land/std/http/file_server.ts";

const server = serve({ port: 8000 });
console.log("http://localhost:8000/");

async function fileExists(path) {
  try {
    const stats = await Deno.lstat(path);
    return stats && stats.isFile;
  } catch(e) {
    if (e && e instanceof Deno.errors.NotFound) {
      return false;
    } else {
      throw e;
    }
  }
}

const signals = Deno.signal(Deno.Signal.SIGINT);
signals.then(signal=>{server.close();signals.dispose()})

for await (const req of server) {
  if(req.url === '/') {
    req.respond({
      status: 307,
      headers: new Headers({
        location: '/test/SpecRunner.html'
      })
    })
    continue;
  }
  const path = `${Deno.cwd()}${req.url}`;
  if (await fileExists(path)) {
    const content = await serveFile(req, path);
    req.respond(content);
    continue;
  }else {
		req.respond({status: 404});
    continue;
	}
}

console.log('Exited')
