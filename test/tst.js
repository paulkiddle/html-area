import HtmlArea from '../src/html-area.js';

HtmlArea.register();

let form;
let ha;

beforeEach(()=>{
	form = document.createElement('form');
	ha = document.createElement('html-area');
	document.body.appendChild(form);
})

afterEach(()=>{
	document.body.removeChild(form);
})

it('Adds innerHTML to form', ()=>{
	ha.setAttribute('name', 'ha');

	ha.innerHTML = '<b>Test</b>';

	form.appendChild(ha);

	const fd = new FormData(form);

	expect(fd.get('ha')).toEqual('<b>Test</b>');

	ha.innerHTML = '<b>Test 2</b>';

	const fd2 = new FormData(form);

	expect(fd2.get('ha')).toEqual('<b>Test 2</b>');
});

it('defaults to inner textarea value if present', () => {
	ha.innerHTML = '<textarea name="ta">Test&lt;hr&gt;</textarea>';

	form.appendChild(ha);

	const fd = new FormData(form);

	expect(fd.get('ta')).toEqual('Test<hr>');
})


it('doesn\'t take name of inner textarea value if explicitly set', () => {
	ha.innerHTML = '<textarea name="ta">Test&lt;hr&gt;</textarea>';
	ha.setAttribute('name', 'ha');
	form.appendChild(ha);

	const fd = new FormData(form);

	expect(fd.get('ta')).toEqual(null);
	expect(fd.get('ha')).toEqual('Test<hr>');
})

it('updates value on input event', () => {
	ha.innerHTML = '';
	ha.setAttribute('name', 'ha');
	form.appendChild(ha);

	ha.appendChild(document.createElement('a'));

	const fd = new FormData(form)
	expect(fd.get('ha')).toEqual('');

	ha.dispatchEvent(new InputEvent('input'))

	const fd2 = new FormData(form);
	expect(fd2.get('ha')).toEqual('<a></a>');
});
