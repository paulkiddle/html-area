# HTMLArea Form CustomElement

A wysiwyg custom element that participates in form submission.
Similar to a textarea but accepts and displays HTML using contenteditable.
Allows a graceful fallback to `<textarea>` for unsupported browsers.

See the <a href="https://paulkiddle.gitlab.io/html-area/">live demo</a> for an example.

Basic usage:

```html
<script type="module">
import HtmlArea from 'https://paulkiddle.gitlab.io/html-area/html-area.js';
HtmlArea.register();
</script>

<form method="post">
<h1>New blog post</h1>
<label>Title <input name="title"></label>
<label>
	Content
	<html-area name="content">
		<p>Post body goes here</p>
	</html-area>
</label>
</form>
```

Usage with fallback textarea:
```html
<script type="module">
import HtmlArea from 'https://paulkiddle.gitlab.io/html-area/html-area.js';
HtmlArea.register();
</script>

<form method="post">
<h1>New blog post</h1>
<label>Title <input name="title"></label>
<label>
	Content
	<html-area>
		<!-- Textarea name and value will be moved to the
		html-area element if the javascript is supported -->
		<textarea name="content">
			&lt;p&gt;Post body goes here&lt;/p&gt;
		</textarea>
	</html-area>
</label>
</form>
```

Includes:
	- Progressive enhancement falling back to a regular text area
	- Adds HTML content to form submission
	- Works with W3C input events (typing, pasting, document.execCommand)
	- Very basic styling

Does not include:
	- Formatting buttons or toolbars
	- HTML validator
	- Advanced styling

This element is to be used as a building block for a more taylored editing experience.
It tries not to make any assumptions about what your applications or users what to do,
instead leaving it up to you to add your own features, just as you would do for a
native `<textarea>` element.

## Usage

### Polyfills

If you want to use this library in a browser that doesn't support [element internals](https://html.spec.whatwg.org/multipage/custom-elements.html#element-internals) (e.g. firefox) you'll have to import a polyfill:

```javascript
import 'https://unpkg.com/element-internals-polyfill@0.1.30/dist/index.js';
```

or

```html
<script type="module" src="https://unpkg.com/element-internals-polyfill@0.1.30/dist/index.js"></script>
```

### Import, register, and basic use

Import the element class and then register it using `customElements.define`, or use the shortcut `register` method:

```javascript
import HtmlArea from 'https://paulkiddle.gitlab.io/html-area/html-area.js';

// Registers with the tag name `html-area`
HtmlArea.register();

// Alternatively you can choose your own tag name:
// HtmlArea.register('my-element');
```

Then you can start using the element in your HTML document:

```html
<form method="POST">
	<html-area name="html_content">
		<p>This is the default content/value for your html area,
			which will be rendered with <code>contenteditable="true"</code>.
		</p>
		<p>You can use <b>any valid HTML</b> in here,
			and special characters like <code>&lt;</code> and <code>&gt;</code> should be escaped</p>
		<p>On browsers that support custom elements and element internals,
			the HTML source will be included in the form data when the form is submitted.
		<p>Browsers that do not support custom elements will just display this HTML</p>
	</html-area>
	<button>Submit</button>
</form>
```

### Progressive enhancement using `<textarea>`

If the first element inside your html-area is a textarea, the contents of html-area
will be replaced with the value of that textarea. This allows the component to
degrade gracefully in unsupported browsers, where users will still be able to submit
HTML via the textarea.

If the textarea has a `name` attribute and the html-area doesn't, that name will be copied
to the html-area.

```html
<html-area>
	<textarea name="myspace_profile_content">
		People say I have a &lt;b&gt;bold&lt;/b&gt; personality!
	</textarea>
</html-area>
```

In supported browsers, the above is equivalent to:

```html
<html-area name="myspace_profile_content">
	People say I have a <b>bold</b> personality!
</html-area>
```

You can also provide different name attributes for your html-area and fallback textarea:

```html
<html-area name="content_html">
	<textarea name="content_markdown">
	</textarea>
	<!--anything after the textarea will be discarded when the custom element activates-->
	<b>Pro tip:</b> Use markdown to add formatting to your content!
</html-area>
```

### Formatting controls and programatic interaction

Since there are so many use cases for an html input element,
it's impossible design formatting controls in a way that suits everyone.

So this element does not include them; you will need to implement them yourself -
but don't worry, it's not that hard! You just need to know a bit about
how the element detects changes.

The html-area will detect changes to `innerHTML` and any input events
(e.g. typing, pasting, or using [`document.execCommand`](https://developer.mozilla.org/en-US/docs/Web/API/document/execCommand)).

It doesn't natively support other types of dom manipulation,
but will pick up changes if you dispatch an
[`InputEvent`](https://www.w3.org/TR/input-events-1/) to it.

```javascript
const area = document.querySelector('html-area');

// This works fine:
area.innerHTML += `<time>${new Date().toISOString()}</time>`;

// As does this (assuming the content has focus):
document.execCommand('insertHTML', `<time>${new Date().toISOString()}</time>`);

// This will not work:
const time = document.createElement('time');
time.innerHTML = new Date().toISOString();
area.appendChild(time);

// ... unless you dispatch an InputEvent afterwards:
area.dispatchEvent(new InputEvent('input'));
```

For example, the very minimum you need to do to add a Bold control is this:

```html
<button type="button" onclick="document.execCommand('bold')">Bold</button>
```

## Development

For development, you need to install Deno.

To run the test suite, first install Jasmine standalone:

```bash
./install-jasmine.sh
```

Then start the test server:

```bash
./test.sh
```

And navigate to `http://localhost:8000/`
